package main

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"encoding/gob"
	"fmt"
	"log"
)

func main() {
	Curve := elliptic.P256()

	private, err := ecdsa.GenerateKey(Curve, rand.Reader)
	if err != nil {
		log.Panic(err)
	}
	var buf bytes.Buffer
	gob.Register(elliptic.P256())
	encode := gob.NewEncoder(&buf)
	err = encode.Encode(private)
	if err != nil {
		fmt.Println(err)
	}
}
